package org.lichtspiele.UUIDHamster.callback.plugin;

import org.bukkit.command.CommandSender;
import org.lichtspiele.UUIDHamster.Messages;
import org.lichtspiele.UUIDHamster.PlayerProfile;
import org.lichtspiele.UUIDHamster.callback.HamsterCallbackInterface;
import org.lichtspiele.UUIDHamster.exception.NoSuchPlayerException;

import java.util.List;

public class HistoryCommandCallback implements HamsterCallbackInterface {

	@Override
	public void finished(CommandSender sender, PlayerProfile profile) {
		Messages.HistoryProfileInformation(sender, profile);
	}

	@Override
	public void finished(CommandSender sender, List<PlayerProfile> profiles) {
		for (PlayerProfile profile : profiles) {
			Messages.HistoryProfileInformation(sender, profile);
		}
	}

	@Override
	public void failed(CommandSender sender, Exception exception) {
		if (exception.getClass().getSimpleName().equals("NoSuchPlayerException")) {
			Messages.ProfileNotFound(sender, (NoSuchPlayerException) exception);
		} else {
			Messages.ApiError(sender, exception);
		}
	}

}
