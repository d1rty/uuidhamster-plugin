package org.lichtspiele.UUIDHamster.callback.plugin;

import org.bukkit.command.CommandSender;
import org.lichtspiele.UUIDHamster.Messages;
import org.lichtspiele.UUIDHamster.PlayerProfile;
import org.lichtspiele.UUIDHamster.UUIDHamster;
import org.lichtspiele.UUIDHamster.callback.HamsterCallbackInterface;
import org.lichtspiele.UUIDHamster.exception.NoSuchPlayerException;

import java.util.ArrayList;
import java.util.List;

public class LookupCommandCallback implements HamsterCallbackInterface {

    @SuppressWarnings("unused")
	private UUIDHamster plugin	= UUIDHamster.getInstance();

	@Override
	public void finished(CommandSender sender, PlayerProfile profile) {
		Messages.BasicProfileInformation(sender, profile);
	}

	public void finished(CommandSender sender, List<PlayerProfile> profiles) {
		for (PlayerProfile profile : profiles) {
			Messages.BasicProfileInformation(sender, profile);
		}
	}

	@Override
	public void failed(CommandSender sender, Exception exception) {
		if (exception.getClass().getSimpleName().equals("NoSuchPlayerException")) {
			Messages.ProfileNotFound(sender, (NoSuchPlayerException) exception);
		} else {
			Messages.ApiError(sender, exception);
		}
	}

}
