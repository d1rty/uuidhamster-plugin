package org.lichtspiele.UUIDHamster.callback.plugin;

import de.cubenation.bedrock.translation.Translation;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.lichtspiele.UUIDHamster.Messages;
import org.lichtspiele.UUIDHamster.PlayerProfile;
import org.lichtspiele.UUIDHamster.UUIDHamster;
import org.lichtspiele.UUIDHamster.callback.HamsterCallbackInterface;
import org.lichtspiele.UUIDHamster.config.Hamster;

import java.util.List;

public class ProfileDeleteCallback implements HamsterCallbackInterface {

    @Override
    public void finished(CommandSender sender, PlayerProfile profile) {
    }

    @Override
    public void finished(CommandSender sender, List<PlayerProfile> profiles) {
        Messages.send(
                UUIDHamster.getInstance(),
                sender,
                new Translation(UUIDHamster.getInstance(), "delete_done").getTranslation())
        ;

        // force save
        if (((Hamster) UUIDHamster.getInstance().getConfigService().getConfig(Hamster.class)).getForceSaveOnDelete()) {
            Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "hamster save");
        }
    }

    @Override
    public void failed(CommandSender sender, Exception exception) {
        Messages.ApiError(sender, exception);
    }
}
