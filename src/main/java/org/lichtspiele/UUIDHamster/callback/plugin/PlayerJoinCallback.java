package org.lichtspiele.UUIDHamster.callback.plugin;

import org.bukkit.command.CommandSender;
import org.lichtspiele.UUIDHamster.PlayerProfile;
import org.lichtspiele.UUIDHamster.UUIDHamster;
import org.lichtspiele.UUIDHamster.cache.UUIDHamsterCache;
import org.lichtspiele.UUIDHamster.callback.HamsterCallbackInterface;

import java.util.List;
import java.util.logging.Level;

public class PlayerJoinCallback implements HamsterCallbackInterface {
	
	private UUIDHamster plugin	= UUIDHamster.getInstance();
	
	@Override
	public void finished(CommandSender sender, PlayerProfile profile) {
		// save profile 
		UUIDHamsterCache.savePlayerProfile(profile.getUsername(), profile);
		UUIDHamsterCache.savePlayerProfile(profile.getUUID(), profile);
	}

	@Override
	public void finished(CommandSender sender, List<PlayerProfile> profiles) {
		System.out.println("this should never happen: playerJoinCallback with set of player profiles");
	}

	@Override
	public void failed(CommandSender sender, Exception exception) {
		this.plugin.log(Level.SEVERE, "PlayerJoinListener throwed exception");
		exception.printStackTrace();
	}

}
