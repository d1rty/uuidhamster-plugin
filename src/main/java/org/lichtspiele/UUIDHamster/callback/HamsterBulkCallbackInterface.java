package org.lichtspiele.UUIDHamster.callback;

import java.util.List;

import org.bukkit.command.CommandSender;
import org.lichtspiele.UUIDHamster.PlayerProfile;

public interface HamsterBulkCallbackInterface {
	
	void finished(CommandSender sender, List<PlayerProfile> profiles);
	
	void failed(CommandSender sender);
	
}
