package org.lichtspiele.UUIDHamster.callback;

import org.bukkit.command.CommandSender;
import org.lichtspiele.UUIDHamster.PlayerProfile;

@SuppressWarnings("unused")
public interface CallbackInterface {
	
	void finished(CommandSender sender, PlayerProfile profile);
	
	void failed(CommandSender sender, Exception e);
	
	
}
