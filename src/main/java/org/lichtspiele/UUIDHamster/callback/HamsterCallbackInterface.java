package org.lichtspiele.UUIDHamster.callback;

import org.bukkit.command.CommandSender;
import org.lichtspiele.UUIDHamster.PlayerProfile;

import java.util.List;

public interface HamsterCallbackInterface {
	
	void finished(CommandSender sender, PlayerProfile profile);

	void finished(CommandSender sender, List<PlayerProfile> profiles);

	void failed(CommandSender sender, Exception e);
	
}
