package org.lichtspiele.UUIDHamster.cache;

import org.lichtspiele.UUIDHamster.PlayerProfile;

import java.util.*;

// implicit synchronized singleton
public class UUIDHamsterCache {

	private static HashMap<String, PlayerProfile> username_data	= new HashMap<>();
	
	private static HashMap<UUID, PlayerProfile> uuid_data		= new HashMap<>();
	
	private static final class InstanceHolder {
	    static final UUIDHamsterCache INSTANCE = new UUIDHamsterCache();
	}

	private UUIDHamsterCache () {}

	public static UUIDHamsterCache getInstance () {
	    return InstanceHolder.INSTANCE;
	}
	
	// boolean exist methods
	public static boolean hasPlayerProfile(String username) {
		return username_data.containsKey(username);
	}
	
	public static boolean hasPlayerProfile(UUID uuid) {
		return uuid_data.containsKey(uuid);
	}
	
	// PlayerProfile return methods
	public static List<PlayerProfile> getPlayerProfile(String username) {
		List<PlayerProfile> profiles = new ArrayList<>();

		if (hasPlayerProfile(username)) {
			profiles.add(username_data.get(username));
		}

		for (Map.Entry pair : uuid_data.entrySet()) {
			PlayerProfile profile = (PlayerProfile) pair.getValue();
			if (profile.getUsername().equals(username)) {
				profiles.add(profile);
			}
		}

		return profiles;
	}
	
	public static PlayerProfile getPlayerProfile(UUID uuid) {
		return (hasPlayerProfile(uuid))
			? uuid_data.get(uuid)
			: null;		
	}

	public static void removePlayerProfile(String username) {
		username_data.remove(username);

		for (Map.Entry pair : uuid_data.entrySet()) {
			PlayerProfile profile = (PlayerProfile) pair.getValue();
			if (profile.getUsername().equals(username)) {
				uuid_data.remove(profile.getUUID());
			}
		}
	}

	public static void removePlayerProfile(UUID uuid) {
		uuid_data.remove(uuid);
	}



	// save PlayerProfile methods
	public static void savePlayerProfile(String username, PlayerProfile profile) {
		username_data.put(username, profile);
	}
	
	public static void savePlayerProfile(UUID uuid, PlayerProfile profile) {
		uuid_data.put(uuid, profile);
	}
	
}
