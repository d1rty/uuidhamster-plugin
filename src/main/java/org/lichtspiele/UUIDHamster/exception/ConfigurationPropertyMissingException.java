package org.lichtspiele.UUIDHamster.exception;

import org.lichtspiele.UUIDHamster.config.Hamster;

public class ConfigurationPropertyMissingException extends Exception {

    private String property;

    public ConfigurationPropertyMissingException(String property) {
        super();
        this.property = property;
    }

    @Override
    public String getMessage() {
        return "Configuration property " + this.property + " is missing in file hamster.yml";
    }

}
