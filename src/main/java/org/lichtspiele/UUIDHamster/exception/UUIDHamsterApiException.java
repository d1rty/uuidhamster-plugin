package org.lichtspiele.UUIDHamster.exception;

public class UUIDHamsterApiException extends IllegalArgumentException {

	private static final long serialVersionUID = -5814219909103893263L;

	public UUIDHamsterApiException(String string) {
		super(string);
	}
	
}
