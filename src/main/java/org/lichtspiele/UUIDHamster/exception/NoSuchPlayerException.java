package org.lichtspiele.UUIDHamster.exception;

public class NoSuchPlayerException extends Exception {
	
	private static final long serialVersionUID = -1599624458732188652L;

	private String player;
	
	public String getPlayer() {
		return this.player;
	}
	
	public void setPlayer(String player) {
		this.player = player;
	}
	
}
