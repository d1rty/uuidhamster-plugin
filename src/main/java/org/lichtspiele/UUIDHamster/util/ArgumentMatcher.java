package org.lichtspiele.UUIDHamster.util;

import java.util.regex.Pattern;

public class ArgumentMatcher {

    public final static Pattern maxPattern          = Pattern.compile("^(m|M):(?<max>.+)$");

    public final static Pattern timestampPattern    = Pattern.compile("^(t|T):(?<timestamp>.+)$");

}
