package org.lichtspiele.UUIDHamster.util;

import java.util.HashMap;
import java.util.UUID;
import java.util.regex.Pattern;

public class UUIDHelper {

    /*
     * cache for succeeded conversions
     */
	public static class cache {
		private static HashMap<String,UUID> uuid_pattern_cache		= new HashMap<>();
		
		public static UUID get(String key) {
			if (cache.uuid_pattern_cache.containsKey(key))
				return cache.uuid_pattern_cache.get(key);
			return null;
		}
		
		public static void set(String key, UUID value) {
			cache.uuid_pattern_cache.put(key, value);
		}
	}

	/*
     * the optional uuid: prefix as Pattern object
     */	
	final static Pattern uuid_prefix =
		Pattern.compile(
			"^" +
			"(?:uuid:)?"
		);
	
    /*
     * lazy Pattern for uuid matching (optional dashes)
     */	
	final static Pattern uuid_pattern_lazy = 
		Pattern.compile(
			"([0-9a-fA-F]{8})-?" +	// 8 chars
			"([0-9a-fA-F]{4})-?" +	// 4 chars
			"([0-9a-fA-F]{4})-?" +	// 4 chars
			"([0-9a-fA-F]{4})-?" +	// 4 chars
			"([0-9a-fA-F]{12})" +	// 12 chars
			"$"
		);

    /*
     * full Pattern for uuid matching
     */	
	final static Pattern uuid_pattern_full = 
		Pattern.compile(
			"([0-9a-fA-F]{8})-" +	// 8 chars
			"([0-9a-fA-F]{4})-" +	// 4 chars
			"([0-9a-fA-F]{4})-" + 	// 4 chars
			"([0-9a-fA-F]{4})-" + 	// 4 chars
			"([0-9a-fA-F]{12})" +	// 12 chars
			"$"
		);		

    /*
     * Checks if a string looks like a UUID string representation
     * To return true, the string must meet length and Pattern match criteria
     *
     * @param String to check
     * @returns true if the string is a UUID string representation, false otherwise
     */
	public static boolean isUUID(String s) {
		if (s == null) return false;
		
		if (
			(s.length() == 36 || s.length() == 41) &&
			s.matches(UUIDHelper.uuid_prefix.toString() + UUIDHelper.uuid_pattern_full.toString())
		)
			return true;
		
		if (	
			(s.length() == 32 || s.length() == 37) &&
			s.matches(UUIDHelper.uuid_prefix.toString() + UUIDHelper.uuid_pattern_lazy.toString())
		) {
			String r = UUIDHelper.UUIDStringRepresentation(s);
			if (r.length() == 36) {
				UUIDHelper.cache.set(s,  UUIDHelper.unsafeToUUID(r));
				return true;
			}
		}
		return false;
	}
	
    /*
     * Creates a string representation from a UUID-like string (with or without dashes)
     *
     * @param UUID lazy string representation
     * @return UUID full string representation 
     */
	public static String UUIDStringRepresentation(String s) {
		return s.replaceAll(UUIDHelper.uuid_prefix.toString() + UUIDHelper.uuid_pattern_lazy.toString(), "$1-$2-$3-$4-$5");
	}
	
    /*
     * Converts a string to a UUID string representation if it matches requirements of isUUID()
     * Returns a UUID object from that string, null for invalid UUID string representation
     *
     * @param lazy/full UUID string representation
     * @return UUID or null
     */
	public static UUID toUUID(String s) {
		if (!UUIDHelper.isUUID(s))
			return null;
					
		if (UUIDHelper.cache.get(s) != null)
			return UUIDHelper.cache.get(s);
		
		return UUID.fromString(UUIDHelper.UUIDStringRepresentation(s));		
	}
	
    /*
     * Returns a UUID object from the string, unsafe
     * This can throw an IllegalArgumentException
     *
     * @return UUID
     */
	public static UUID unsafeToUUID(String s) {
		return UUID.fromString(s);
	}
	
}
