package org.lichtspiele.UUIDHamster.config;

import de.cubenation.bedrock.BasePlugin;
import de.cubenation.bedrock.service.config.CustomConfigurationFile;
import net.cubespace.Yamler.Config.Path;

import java.io.File;

public class Hamster extends CustomConfigurationFile {

    public static String getFilename() {
        return "hamster.yml";
    }

    public Hamster(BasePlugin plugin) {
        this(plugin, getFilename());
    }

    public Hamster(BasePlugin plugin, String name) {
        CONFIG_FILE = new File(plugin.getDataFolder(), name);
    }

    @Path("api.key")
    private String api_key                  = "";

    public String getApiKey() {
        return this.api_key;
    }


    @Path("api.uri")
    private String api_uri                  = "";

    public String getApiUri() {
        return this.api_uri;
    }


    @Path("save_interval")
    private int save_interval               = 300;

    public int getSaveInterval() {
        return this.save_interval;
    }


    @Path("date_format")
    private String date_format              = "dd.MM.YYYY HH:mm:ss";

    public String getDateFormat() {
        return this.date_format;
    }


    @Path("max_results")
    private Integer max_results             = 1;

    public Integer getMaxResults() { return this.max_results; }


    @Path("force_save_on_delete")
    private boolean force_save_on_delete    = true;

    public boolean getForceSaveOnDelete() { return this.force_save_on_delete; }

}
