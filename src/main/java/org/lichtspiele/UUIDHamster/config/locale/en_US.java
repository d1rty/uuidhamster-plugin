package org.lichtspiele.UUIDHamster.config.locale;

import de.cubenation.bedrock.BasePlugin;
import de.cubenation.bedrock.service.config.CustomConfigurationFile;
import net.cubespace.Yamler.Config.Path;

import java.io.File;

@SuppressWarnings("unused")
public class en_US extends CustomConfigurationFile {

    public static String getFilename() {
        return "locale" + File.separator + "en_US.yml";
    }

    public en_US(BasePlugin plugin) {
        CONFIG_FILE = new File(plugin.getDataFolder(), getFilename());
	}

    @Path("api_error")
    private String api_error                    = "%plugin_prefix%&RESET& &RED&API Error";

    @Path("profile_not_found")
    private String profile_not_found            = "%plugin_prefix%&RESET& &SECONDARY&Player &PRIMARY&%player% &SECONDARY&not found";

    @Path("username")
	private String username                     = "&SECONDARY&Username: &PRIMARY&%username%&SECONDARY&";

    @Path("uuid")
    private String uuid                         = "&SECONDARY&UUID: &PRIMARY&%uuid%&SECONDARY&";

    @Path("lookup")
    private String lookup                       = "%plugin_prefix%&RESET& &SECONDARY&Player &PRIMARY&%username%&SECONDARY&:";

    @Path("history")
    private String history                      = "%plugin_prefix%&RESET& &SECONDARY&History for &PRIMARY&%username%&SECONDARY&:";

    @Path("no_history")
    private String no_history                   = "&FLAG&No history available";

    @Path("history_entry")
    private String history_entry                = "&SECONDARY& - %username%";

    @Path("history_change_amount")
    private String history_change_amount        = "&SECONDARY&%amount% name change(s)";

    @Path("history_name_change_at")
	private String history_name_change_at       = "&FLAG&Change at: %at%&SECONDARY&";

    @Path("no_change_date")
    private String no_change_date               = "&FLAG&No change at&SECONDARY&";

    @Path("save_done")
	private String save_done                    = "%plugin_prefix%&RESET& Hamster Data saved";

    @Path("delete_done")
    private String delete_done                  = "%plugin_prefix%&RESET& Hamster Data deleted";

    @Path("help.lookup")
    private String help_lookup                  = "Lookup a user by username or UUID";

    @Path("help.history")
    private String help_history                 = "Display history of a username or UUID";

    @Path("help.save")
    private String help_save                    = "Forces saving of Hamster data";

    @Path("help.delete")
    private String help_delete                  = "Delete all Hamster data for a player";

    @Path("help.args.username_uuid.description")
    private String help_args_username_uuid_description  = "Username or UUID";

    @Path("help.args.username_uuid.placeholder")
    private String help_args_username_uuid_placeholder  = "username/uuid";

    @Path("help.args.max.description")
    private String help_args_max_description    = "Maximum results (0 = no limit)";

    @Path("help.args.max.placeholder")
    private String help_args_max_placeholder    = "m:<max>";

    @Path("help.args.timestamp.description")
    private String help_args_timestamp_description      = "UNIX Timestamp";

    @Path("help.args.timestamp.placeholder")
    private String help_args_timestamp_placeholder      = "t:<timestamp>";

}
