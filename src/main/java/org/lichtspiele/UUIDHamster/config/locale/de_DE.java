package org.lichtspiele.UUIDHamster.config.locale;

import de.cubenation.bedrock.BasePlugin;
import de.cubenation.bedrock.service.config.CustomConfigurationFile;
import net.cubespace.Yamler.Config.Path;

import java.io.File;

@SuppressWarnings("unused")
public class de_DE extends CustomConfigurationFile {

    public static String getFilename() {
        return "locale" + File.separator + "de_DE.yml";
    }

	public de_DE(BasePlugin plugin) {
        CONFIG_FILE = new File(plugin.getDataFolder(), getFilename());
	}

    @Path("api_error")
    private String api_error                    = "%plugin_prefix%&RESET& &RED&API Fehler";

    @Path("profile_not_found")
    private String profile_not_found            = "%plugin_prefix%&RESET& &SECONDARY&Spieler &PRIMARY&%player% &SECONDARY&nicht gefunden";

    @Path("username")
    private String username                     = "&SECONDARY&Spieler: &PRIMARY&%username%&SECONDARY&";

    @Path("uuid")
    private String uuid                         = "&SECONDARY&UUID: &PRIMARY&%uuid%&SECONDARY&";

    @Path("lookup")
    private String lookup                       = "%plugin_prefix%&RESET& &SECONDARY&Spieler &PRIMARY&%username%&SECONDARY&:";

    @Path("history")
    private String history                      = "%plugin_prefix%&RESET& &SECONDARY&Historie für &PRIMARY&%username%&SECONDARY&:";

    @Path("no_history")
    private String no_history                   = "&FLAG&Keine Namensänderung";

    @Path("history_entry")
    private String history_entry                = "&SECONDARY& - %username%";

    @Path("history_change_amount")
    private String history_change_amount        = "&SECONDARY&%amount% Namensänderung(en)";

    @Path("history_name_change_at")
    private String history_name_change_at       = "&FLAG&Änderungsdatum: %at%&SECONDARY&";

    @Path("no_change_date")
    private String no_change_date               = "&FLAG&Kein Änderungsdatum&SECONDARY&";

    @Path("save_done")
    private String save_done                    = "%plugin_prefix%&RESET& Hamster Daten gespeichert";

    @Path("delete_done")
    private String delete_done                  = "%plugin_prefix%&RESET& Hamster Daten gelöscht";

    @Path("help.lookup")
    private String help_lookup                  = "Informationen zu einem Spieler/einer UUID";

    @Path("help.history")
    private String help_history                 = "Namenshistorie zu einem Spieler/einer UUID anzeigen";

    @Path("help.save")
    private String help_save                    = "Speichert alle Hamster Daten";

    @Path("help.delete")
    private String help_delete                  = "Löscht alle Hamster Daten eines Spielers";

    @Path("help.args.username_uuid.description")
    private String help_args_username_uuid_description  = "Username oder UUID";

    @Path("help.args.username_uuid.placeholder")
    private String help_args_username_uuid_placeholder  = "username/uuid";
    
    @Path("help.args.max.description")
    private String help_args_max_description    = "Maximale Anzahl Ergebnisse (0 = kein Limit)";

    @Path("help.args.max.placeholder")
    private String help_args_max_placeholder    = "m:<max>";

    @Path("help.args.timestamp.description")
    private String help_args_timestamp_description      = "UNIX Timestamp";

    @Path("help.args.timestamp.placeholder")
    private String help_args_timestamp_placeholder      = "t:<timestamp>";
    
}
