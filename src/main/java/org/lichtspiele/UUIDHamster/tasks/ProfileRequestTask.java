package org.lichtspiele.UUIDHamster.tasks;

import org.bukkit.command.CommandSender;
import org.bukkit.scheduler.BukkitRunnable;
import org.lichtspiele.UUIDHamster.api.UUIDHamsterAPIConnector;
import org.lichtspiele.UUIDHamster.PlayerProfile;
import org.lichtspiele.UUIDHamster.UUIDHamster;
import org.lichtspiele.UUIDHamster.cache.UUIDHamsterCache;
import org.lichtspiele.UUIDHamster.callback.HamsterBulkCallbackInterface;
import org.lichtspiele.UUIDHamster.callback.HamsterCallbackInterface;
import org.lichtspiele.UUIDHamster.exception.NoSuchPlayerException;
import org.lichtspiele.UUIDHamster.exception.RateLimitException;
import org.lichtspiele.UUIDHamster.exception.UUIDHamsterApiException;

import java.util.*;
import java.util.logging.Level;

public class ProfileRequestTask {
		
	public void run(final CommandSender sender, final UUID uuid, final HamsterCallbackInterface callback) {
		new BukkitRunnable() {
			public void run() {

				try {
					PlayerProfile profile = UUIDHamsterAPIConnector.profile(uuid);
					UUIDHamsterCache.savePlayerProfile(uuid, profile);
					callback.finished(sender, profile);
				
				} catch (NoSuchPlayerException e) {
					e.setPlayer(uuid.toString());
					callback.failed(sender, e);
					
				} catch(UUIDHamsterApiException e) {
					callback.failed(sender, e);
				}
			
			}
		}.runTaskAsynchronously(UUIDHamster.getInstance());
	}
	
	public void run(final CommandSender sender, final String username, final Integer max_results, final Date at, final HamsterCallbackInterface callback) {
		new BukkitRunnable() {
			public void run() {

				try {
					List<PlayerProfile> profiles = UUIDHamsterAPIConnector.profile(username, max_results, at);

					for (PlayerProfile profile : profiles) {
						UUIDHamsterCache.savePlayerProfile(username, profile);
					}

					callback.finished(sender, profiles);
				
				} catch (NoSuchPlayerException e) {
					e.setPlayer(username);
					callback.failed(sender, e);				
					
				} catch (UUIDHamsterApiException e) {
					callback.failed(sender, e); 

				}
				
			}
		}.runTaskAsynchronously(UUIDHamster.getInstance());
	}
	
	public void run(final CommandSender sender, final Set<?> objects, final Integer max_results, final Date at, final HamsterBulkCallbackInterface callback) {
		new BukkitRunnable() {

			@Override
			public void run() {
				
				List<PlayerProfile> profiles = new ArrayList<PlayerProfile>();
				
				for (Object object : objects) {
					// check for compatible types
					if ( !(object instanceof String) && !(object instanceof UUID) ) {
						continue;
					}

					// try to get player profiles from local cache
					if (object instanceof UUID && UUIDHamsterCache.hasPlayerProfile((UUID) object)) {
						profiles.add(UUIDHamsterCache.getPlayerProfile((UUID) object));
						continue;
					}

					if (object instanceof String && UUIDHamsterCache.hasPlayerProfile((String) object)) {
						profiles.addAll(UUIDHamsterCache.getPlayerProfile((String) object));
						continue;
					}


					// fetch player profile from UUIDHamster backend API
					try {

                        if (object instanceof UUID) {
                            PlayerProfile profile = UUIDHamsterAPIConnector.profile((UUID) object);
                            UUIDHamsterCache.savePlayerProfile((UUID) object, profile);

                            profiles.add(profile);

                        } else {
                            for (PlayerProfile profile : UUIDHamsterAPIConnector.profile((String) object, max_results, at)) {
                                UUIDHamsterCache.savePlayerProfile(profile.getUsername(), profile);

                                profiles.add(profile);
                            }
                        }

					} catch (RateLimitException e) {
						UUIDHamster.getInstance().log(Level.SEVERE, "Rate-Limit exception");
						break;
						
					} catch (UUIDHamsterApiException e) {
						UUIDHamster.getInstance().log(Level.SEVERE, "UUIDHamster API throwed error: " + e.getMessage());
						break;

					} catch (NoSuchPlayerException ignored) {
					}
				}
				
				if (profiles.size() == 0) {
					callback.failed(sender);
				} else {
					callback.finished(sender, profiles);					
				}
			}
			
		}.runTaskAsynchronously(UUIDHamster.getInstance());
	}

}