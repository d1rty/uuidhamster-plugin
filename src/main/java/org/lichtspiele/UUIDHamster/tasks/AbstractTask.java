package org.lichtspiele.UUIDHamster.tasks;

public abstract class AbstractTask {
	
	public abstract void run();
	
}
