package org.lichtspiele.UUIDHamster.tasks;

import org.bukkit.scheduler.BukkitRunnable;

import org.lichtspiele.UUIDHamster.api.UUIDHamsterAPIConnector;
import org.lichtspiele.UUIDHamster.UUIDHamster;

public class SaveTask extends AbstractTask {
	
	@Override
	public void run() {
		new BukkitRunnable() {
			public void run() {
				UUIDHamsterAPIConnector.save();
			}
		}.runTaskAsynchronously(UUIDHamster.getInstance());
	}
	
}