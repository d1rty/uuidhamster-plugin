package org.lichtspiele.UUIDHamster.tasks;

import org.bukkit.command.CommandSender;
import org.bukkit.scheduler.BukkitRunnable;
import org.lichtspiele.UUIDHamster.api.UUIDHamsterAPIConnector;
import org.lichtspiele.UUIDHamster.PlayerProfile;
import org.lichtspiele.UUIDHamster.UUIDHamster;
import org.lichtspiele.UUIDHamster.callback.HamsterCallbackInterface;
import org.lichtspiele.UUIDHamster.exception.UUIDHamsterApiException;

import java.util.ArrayList;
import java.util.Set;
import java.util.UUID;

public class ProfileDeleteTask {
		
	public void run(final CommandSender sender, final Set<Object> idents, final HamsterCallbackInterface callback) {
		new BukkitRunnable() {
			public void run() {
                UUIDHamsterApiException exception = null;
                for (Object ident : idents) {

                    if (ident instanceof UUID) {
                        ident = ident.toString();
                    }

                    try {
                        UUIDHamsterAPIConnector.remove((String) ident);
                    } catch (UUIDHamsterApiException e) {
                        exception = e;
                    }
                }

                if (exception == null) {
                    callback.finished(sender, new ArrayList<PlayerProfile>());
                } else {
                    callback.failed(sender, exception);
                }

			}
		}.runTaskAsynchronously(UUIDHamster.getInstance());
	}

}