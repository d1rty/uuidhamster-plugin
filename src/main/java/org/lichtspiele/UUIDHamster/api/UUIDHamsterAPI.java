package org.lichtspiele.UUIDHamster.api;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.lichtspiele.UUIDHamster.PlayerProfile;
import org.lichtspiele.UUIDHamster.UUIDHamster;
import org.lichtspiele.UUIDHamster.cache.UUIDHamsterCache;
import org.lichtspiele.UUIDHamster.callback.HamsterBulkCallbackInterface;
import org.lichtspiele.UUIDHamster.callback.HamsterCallbackInterface;
import org.lichtspiele.UUIDHamster.config.Hamster;
import org.lichtspiele.UUIDHamster.tasks.ProfileDeleteTask;
import org.lichtspiele.UUIDHamster.tasks.ProfileRequestTask;

import java.util.*;

public class UUIDHamsterAPI {

	private Integer max_results     = ((Hamster) UUIDHamster.getInstance().getConfigService().getConfig(Hamster.class)).getMaxResults();

	private Date at                 = new Date();

    private Date controlDate        = new Date();

	public UUIDHamsterAPI() {

	}

	public UUIDHamsterAPI max(Integer max_results) {
		this.max_results = max_results;
        return this;
	}

	public UUIDHamsterAPI at(Date at) {
		this.at = at;
		return this;
	}

	public List<PlayerProfile> profile(final String username, final HamsterCallbackInterface callback) {
		return profile(null, username, callback);
	}
	
	public List<PlayerProfile> profile(final CommandSender sender, final String username, final HamsterCallbackInterface callback) {

        final Integer max_results = this.max_results;
        final Date at = this.at;

        if (    at.equals(controlDate) &&
                UUIDHamsterCache.hasPlayerProfile(username) &&
                UUIDHamsterCache.getPlayerProfile(username).size() == this.max_results
                ) {
            return UUIDHamsterCache.getPlayerProfile(username);
        }

		Bukkit.getServer().getScheduler().runTaskAsynchronously(UUIDHamster.getInstance(), new Runnable() {
			@Override
			public void run() {
				ProfileRequestTask task = new ProfileRequestTask();
				task.run(sender, username, max_results, at, callback);
			}
		});		
		return null;
	}
	
	public PlayerProfile profile(UUID uuid, HamsterCallbackInterface callback) {
		return profile(null, uuid, callback);
	}
	
	public PlayerProfile profile(final CommandSender sender, final UUID uuid, final HamsterCallbackInterface callback) {
		if (UUIDHamsterCache.hasPlayerProfile(uuid)) {
			return UUIDHamsterCache.getPlayerProfile(uuid);
		}
		
		Bukkit.getServer().getScheduler().runTaskAsynchronously(UUIDHamster.getInstance(), new Runnable() {
			@Override
			public void run() {
				ProfileRequestTask task = new ProfileRequestTask();
				task.run(sender, uuid, callback);
			}
		});
		return null;	
	}

	@SuppressWarnings("unused")
	public void profiles(final CommandSender sender, final Set<?> objects, final HamsterBulkCallbackInterface callback) {
        final Integer max_results = this.max_results;
        final Date at = this.at;

        Bukkit.getServer().getScheduler().runTaskAsynchronously(UUIDHamster.getInstance(), new Runnable() {
			@Override
			public void run() {
				ProfileRequestTask task = new ProfileRequestTask();
				task.run(sender, objects, max_results, at, callback);
			}
		});		
	}

    @SuppressWarnings("unused")
	public void delete(final CommandSender sender, final UUID uuid, final HamsterCallbackInterface callback) {
        final Set<Object> items = new HashSet<>();
        items.add(uuid);

		Bukkit.getServer().getScheduler().runTaskAsynchronously(UUIDHamster.getInstance(), new Runnable() {
			@Override
			public void run() {
                ProfileDeleteTask task = new ProfileDeleteTask();
                task.run(sender, items, callback);
			}
		});
	}

    public void delete(final CommandSender sender, final String username, final HamsterCallbackInterface callback) {
        final Set<Object> items = new HashSet<>();
        items.add(username);

        for (PlayerProfile profile : UUIDHamsterCache.getPlayerProfile(username)) {
            items.add(profile.getUsername());
        }

        Bukkit.getServer().getScheduler().runTaskAsynchronously(UUIDHamster.getInstance(), new Runnable() {
            @Override
            public void run() {
                ProfileDeleteTask task = new ProfileDeleteTask();
                task.run(sender, items, callback);
            }
        });
    }
	
}