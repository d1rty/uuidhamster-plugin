package org.lichtspiele.UUIDHamster.api;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.lichtspiele.UUIDHamster.PlayerProfile;
import org.lichtspiele.UUIDHamster.UUIDHamster;
import org.lichtspiele.UUIDHamster.config.Hamster;
import org.lichtspiele.UUIDHamster.exception.NoSuchPlayerException;
import org.lichtspiele.UUIDHamster.exception.RateLimitException;
import org.lichtspiele.UUIDHamster.exception.UUIDHamsterApiException;
import org.lichtspiele.UUIDHamster.util.UUIDHelper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;
import java.util.logging.Level;

public class UUIDHamsterAPIConnector {

	private enum ApiMethod {
		GET, PUT, POST, DELETE
	}

	private static UUIDHamster plugin				= (UUIDHamster) UUIDHamster.getInstance();

	private static String api_uri;

	private static String api_key;

	static {
		try {
            api_uri = ((Hamster) UUIDHamster.getInstance().getConfigService().getConfig(Hamster.class)).getApiUri();
			if (api_uri.isEmpty()) throw new IllegalArgumentException("API URI must not be empty");

			//noinspection deprecation
			api_key = ((Hamster) UUIDHamster.getInstance().getConfigService().getConfig(Hamster.class)).getApiKey();
			if (api_key.isEmpty()) throw new IllegalArgumentException("API Key must not be empty");
		} catch (IllegalArgumentException e) {
			plugin.disable(e);
		}
	}

	// 04.02.2014 00:00:00 UTC
    @SuppressWarnings("unused")
	public final static Date critical_date		    = new Date((long) 1423004400 * 1000);
	
    private static final JSONParser json_parser	    = new JSONParser();

    private static HttpURLConnection createConnection(URL url) throws IOException {
    	return createConnection(url, ApiMethod.GET);
    }
    
    private static HttpURLConnection createConnection(URL url, ApiMethod method) throws IOException {
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod(method.name());
        connection.setRequestProperty("Content-Type", "application/json");
        connection.setRequestProperty("Accept", "application/json");
        connection.setRequestProperty("X-HamsterAPI-Key", api_key);
        connection.setUseCaches(false);
        connection.setDoInput(true);
        connection.setDoOutput(true);
        return connection;
    }

    private static void writeBody(HttpURLConnection connection, String body) throws IOException {
        OutputStream stream = connection.getOutputStream();
        stream.write(body.getBytes());
        stream.flush();
        stream.close();
    }	

    
	/*
	 * Return profile for a given username to a given time
	 * 
	 * @param String username
	 * @param Date timestamp
	 */
	public static List<PlayerProfile> profile(String username, Integer max_results, Date at)
		throws NoSuchPlayerException, RateLimitException, UUIDHamsterApiException {

		// build query string
        ArrayList<String> query_array = new ArrayList<>();

        // max results
        if (max_results == null) {
            query_array.add("max=" + ((Hamster) UUIDHamster.getInstance().getConfigService().getConfig(Hamster.class)).getMaxResults().toString());
        } else {
            query_array.add("max=" + max_results.toString());
        }

		// at timestamp
		if (at == null) {
			at = new Date();
		}
        query_array.add("at=" + String.valueOf(at.getTime() / 1000));

        String query 	= "?" + StringUtils.join(query_array, "&");

		// retrieve profile from API
		try {
			URL url = new URL(api_uri + "/profile/username/" + username + query);
		
			HttpURLConnection connection = createConnection(url);

			// empty response 
			if (connection.getResponseCode() == 404) {
				throw new NoSuchPlayerException();
			}
			
			if (connection.getResponseCode() == 429) {
				throw new RateLimitException();
			}

			InputStreamReader in = new InputStreamReader(connection.getInputStream());

			BufferedReader reader = new BufferedReader(in);
			StringBuilder out = new StringBuilder();
			String line;
			while ((line = reader.readLine()) != null) {
				out.append(line);
			}
			reader.close();
            connection.disconnect();

			// debug
			UUIDHamster.getInstance().log(Level.FINE, "Queried Hamster Backend API for " + username + ". Result is: " + out.toString());

			JSONArray json_array_data;
			try {
				json_array_data = (JSONArray) json_parser.parse(out.toString());
			} catch (ParseException e) {
				UUIDHamster.getInstance().log(Level.SEVERE, "Error parsing JSON string: " + e.getMessage());
				UUIDHamster.getInstance().log(Level.SEVERE, out.toString());
				throw new UUIDHamsterApiException(e.getMessage());
			}

            List<PlayerProfile> profiles = new ArrayList<>();
            for (Object json_object_data : json_array_data) {
                profiles.add(new PlayerProfile((JSONObject) json_object_data));
            }

			return profiles;
		
		} catch (IOException e) {
			throw new UUIDHamsterApiException(e.getMessage());
		}		
	}
	
	
	/*  
	 * Return the name history of a given UUID
	 * 
	 * Currently works only for accounts where with no name changes - whyever
	 * But cool for us, we can save them as they come 
	 * 
	 * @param UUID uuid
	 */
	public static PlayerProfile profile(UUID uuid)
		throws NoSuchPlayerException, RateLimitException, UUIDHamsterApiException {		
		
		try {
			URL url = new URL(api_uri + "/profile/uuid/" + uuid.toString());
			HttpURLConnection connection = createConnection(url);
	
			if (connection.getResponseCode() == 404) {
				throw new NoSuchPlayerException();
			}
			
			if (connection.getResponseCode() == 429) {
				throw new RateLimitException();
			}

			InputStreamReader in = new InputStreamReader(connection.getInputStream());

			BufferedReader reader = new BufferedReader(in);
			StringBuilder out = new StringBuilder();
			String line;
			while ((line = reader.readLine()) != null) {
				out.append(line);
			}
			reader.close();
            connection.disconnect();

            // debug
            UUIDHamster.getInstance().log(Level.FINE, "Queried Hamster Backend API for " + uuid.toString() + ". Result is: " + out.toString());

			JSONObject parsed;
			try {
				parsed = (JSONObject) json_parser.parse(out.toString());
			} catch (ParseException e) {
				UUIDHamster.getInstance().log(Level.SEVERE, "Error parsing JSON string: " + e.getMessage());
				UUIDHamster.getInstance().log(Level.SEVERE, out.toString());
				throw new UUIDHamsterApiException(e.getMessage());
			}

			return new PlayerProfile(parsed);
			
		} catch (IOException e) {
			throw new UUIDHamsterApiException(e.getMessage());
		}		
	}

	@SuppressWarnings("unused")
	public static ArrayList<PlayerProfile> resolveBulk(String username) throws IllegalArgumentException {
		return resolveBulk(Collections.singletonList(username));
	}	
	

	@SuppressWarnings("unchecked")
	public static ArrayList<PlayerProfile> resolveBulk(List<String> usernames) throws IllegalArgumentException {
		if (usernames.size() > 100)
			throw new IllegalArgumentException("maximum of 100 usernames allowed for bulk requests");
		
		try {
			URL url = new URL(api_uri + "/profiles");
			HttpURLConnection connection = createConnection(url, ApiMethod.POST);
			
			// create json input and write to http request body
			JSONArray input = new JSONArray();
			for (String u : usernames) {
				input.add(u);
			}
			writeBody(connection, input.toJSONString());
			
			if (connection.getResponseCode() == 429) {
                connection.disconnect();
				throw new RateLimitException();
			}
			
			// parse result data
			JSONArray result = (JSONArray) json_parser.parse(
				new InputStreamReader(connection.getInputStream())
			);
            connection.disconnect();

			// create array with profiles
			ArrayList<PlayerProfile> profiles = new ArrayList<>();
			for (Object aResult : result) {
				profiles.add(new PlayerProfile((JSONObject) aResult));
			}
			return profiles;
			
		} catch (IOException | ParseException e) {
			throw new UUIDHamsterApiException(e.getMessage());
		}
	}

	
	public static void save() throws UUIDHamsterApiException {
        HttpURLConnection connection = null;
		try {
			URL url = new URL(api_uri + "/save");
			connection = createConnection(url, ApiMethod.PUT);

			if (connection.getResponseCode() != 200)
				throw new UUIDHamsterApiException(connection.getResponseMessage());
			
		} catch (Exception e) {
            if (connection != null) connection.disconnect();
			throw new UUIDHamsterApiException(e.getMessage());
		}
        connection.disconnect();
	}


    public static void remove(String ident) throws UUIDHamsterApiException {
        HttpURLConnection connection = null;

        try {
            URL url = new URL(api_uri + "/" + ident);
            connection = createConnection(url, ApiMethod.DELETE);

            if (connection.getResponseCode() != 200)
                throw new UUIDHamsterApiException(connection.getResponseMessage());

        } catch (Exception e) {
            if (connection != null) connection.disconnect();
            throw new UUIDHamsterApiException(e.getMessage());
        }
        connection.disconnect();
    }

}