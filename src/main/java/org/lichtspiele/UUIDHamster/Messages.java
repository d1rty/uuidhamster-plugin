package org.lichtspiele.UUIDHamster;

import de.cubenation.bedrock.helper.MessageHelper;
import de.cubenation.bedrock.translation.Translation;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.command.CommandSender;
import org.lichtspiele.UUIDHamster.config.Hamster;
import org.lichtspiele.UUIDHamster.exception.NoSuchPlayerException;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

public class Messages extends MessageHelper {

    protected static UUIDHamster plugin                 = UUIDHamster.getInstance();

    private static SimpleDateFormat date_format;

    static {
        try {
            date_format = new SimpleDateFormat(((Hamster) plugin.getConfigService().getConfig(Hamster.class)).getDateFormat());
        } catch (NullPointerException e) {
            plugin.disable(e);
        }
    }

    public static void ApiError(CommandSender sender, Exception exception) {
        send(plugin, sender, new Translation(plugin, "api_error").getTranslation());
        exception.printStackTrace();
    }

    public static void ProfileNotFound(CommandSender sender, NoSuchPlayerException exception) {
        send(plugin, sender, new Translation(
                plugin,
                "profile_not_found",
                new String[]{"player", exception.getPlayer()}
        ).getTranslation());
    }

    public static void BasicProfileInformation(CommandSender sender, final PlayerProfile profile) {
        // build header
        TextComponent header = new TextComponent(
                new Translation(plugin, "lookup", new String[]{"username", profile.getUsername()}).getTranslation()
        );
        HoverEvent header_hover_event = new HoverEvent(HoverEvent.Action.SHOW_TEXT,
                TextComponent.fromLegacyText(new Translation(
                        plugin,
                        "history_change_amount",
                        new String[]{"amount", String.valueOf(profile.getHistory().size() - 1)}
                ).getTranslation())
        );
        ClickEvent header_click_event = new ClickEvent(
                ClickEvent.Action.SUGGEST_COMMAND,
                "/hamster h " + profile.getUUID().toString()
        );
        // send components
        send(plugin, sender, header, header_hover_event, header_click_event);

        send(plugin, sender, getUsernameTextComponent(profile), null, null);
        send(plugin, sender, getUUIDTextComponent(profile), null, null);
    }


    public static void HistoryProfileInformation(CommandSender sender, PlayerProfile profile) {

        // build header
        TextComponent header = new TextComponent(
                new Translation(plugin, "history", new String[]{"username", profile.getUsername()}).getTranslation()
        );
        HoverEvent header_hover_event = new HoverEvent(
                HoverEvent.Action.SHOW_TEXT,
                TextComponent.fromLegacyText(getUUIDTextComponent(profile).toLegacyText())
        );
        ClickEvent header_click_event = new ClickEvent(
                ClickEvent.Action.SUGGEST_COMMAND,
                "/hamster l " + profile.getUUID().toString()
        );
        // send components
        send(plugin, sender, header, header_hover_event, header_click_event);


        // check if there are no changes
        if (!profile.hasHistoricUsernames()) {
            send(plugin, sender, new Translation(plugin, "no_history").getTranslation());
            return;
        }


        // get sorted map
        Map<Date, String> history = profile.getHistory();

        // iterate over all entries and build text components
        for (Map.Entry<Date, String> dateStringEntry : history.entrySet()) {
            Map.Entry pair = (Map.Entry) dateStringEntry;

            TextComponent change = new TextComponent(" - " + pair.getValue().toString());
            HoverEvent change_hover_event;

            // this username was never changed
            if (pair.getKey() == null) {
                change_hover_event = new HoverEvent(
                        HoverEvent.Action.SHOW_TEXT,
                        TextComponent.fromLegacyText(
                                new Translation(plugin, "no_change_date").getTranslation()
                        )
                );

            // this username has been changed
            } else {
                change_hover_event = new HoverEvent(
                        HoverEvent.Action.SHOW_TEXT,
                        TextComponent.fromLegacyText(
                                new Translation(
                                        plugin,
                                        "history_name_change_at",
                                        new String[]{ "at", date_format.format(pair.getKey()) }
                                ).getTranslation()
                        )
                );

            }

            // send components
            send(plugin, sender, change, change_hover_event, null);
        }
    }


    private static TextComponent getUUIDTextComponent(PlayerProfile profile) {
        return  new TextComponent(
                new Translation(
                        plugin,
                        "uuid",
                        new String[] { "uuid", profile.getUUID().toString() }
                ).getTranslation()
        );
    }

    private static TextComponent getUsernameTextComponent(PlayerProfile profile) {
        return  new TextComponent(
                new Translation(
                        plugin,
                        "username",
                        new String[] { "username", profile.getUsername() }
                ).getTranslation()
        );
    }

}
