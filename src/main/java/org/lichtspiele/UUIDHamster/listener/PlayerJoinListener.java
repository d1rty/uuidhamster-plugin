package org.lichtspiele.UUIDHamster.listener;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.lichtspiele.UUIDHamster.UUIDHamster;
import org.lichtspiele.UUIDHamster.api.UUIDHamsterAPI;
import org.lichtspiele.UUIDHamster.cache.UUIDHamsterCache;
import org.lichtspiele.UUIDHamster.callback.plugin.PlayerJoinCallback;

public class PlayerJoinListener implements Listener {

	public static UUIDHamster plugin	= (UUIDHamster) UUIDHamster.getInstance();

	@EventHandler(priority=EventPriority.HIGHEST)
	public void onPlayerJoinEvent(PlayerJoinEvent event) {
		Player p = event.getPlayer();
		
		if (UUIDHamsterCache.hasPlayerProfile(p.getUniqueId()))
			return;
		
		// initialize the players profile
		new UUIDHamsterAPI().profile(p.getUniqueId(), new PlayerJoinCallback());
	}
	
}