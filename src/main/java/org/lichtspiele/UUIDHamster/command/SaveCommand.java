package org.lichtspiele.UUIDHamster.command;

import de.cubenation.bedrock.BasePlugin;
import de.cubenation.bedrock.command.Command;
import de.cubenation.bedrock.command.argument.Argument;
import de.cubenation.bedrock.command.manager.CommandManager;
import de.cubenation.bedrock.exception.CommandException;
import de.cubenation.bedrock.exception.IllegalCommandArgumentException;
import de.cubenation.bedrock.helper.MessageHelper;
import de.cubenation.bedrock.permission.Permission;
import de.cubenation.bedrock.translation.Translation;
import org.bukkit.command.CommandSender;
import org.lichtspiele.UUIDHamster.api.UUIDHamsterAPIConnector;

import java.util.ArrayList;

public class SaveCommand extends Command {

	public SaveCommand(BasePlugin plugin, CommandManager commandManager) {
		super(plugin, commandManager);
	}

	@Override
	public void setPermissions(ArrayList<Permission> permissions) {
		permissions.add(new Permission("save"));
	}

	@Override
	public void setSubCommands(ArrayList<String[]> subCommands) {
		subCommands.add(new String[] { "save", "s" } );
	}

	@Override
	public void setDescription(StringBuilder description) {
		description.append("help.save");
	}

	@Override
	public void setArguments(ArrayList<Argument> arguments) {
	}

	@Override
	public void execute(CommandSender sender, String[] subcommands, String[] args) throws CommandException, IllegalCommandArgumentException {
        UUIDHamsterAPIConnector.save();
		MessageHelper.send(plugin, sender, new Translation(this.plugin, "save_done").getTranslation());
	}

}