package org.lichtspiele.UUIDHamster.command;

import de.cubenation.bedrock.BasePlugin;
import de.cubenation.bedrock.command.Command;
import de.cubenation.bedrock.command.argument.Argument;
import de.cubenation.bedrock.command.manager.CommandManager;
import de.cubenation.bedrock.exception.CommandException;
import de.cubenation.bedrock.exception.IllegalCommandArgumentException;
import de.cubenation.bedrock.permission.Permission;
import org.bukkit.command.CommandSender;
import org.lichtspiele.UUIDHamster.PlayerProfile;
import org.lichtspiele.UUIDHamster.api.UUIDHamsterAPI;
import org.lichtspiele.UUIDHamster.callback.plugin.LookupCommandCallback;
import org.lichtspiele.UUIDHamster.util.ArgumentMatcher;
import org.lichtspiele.UUIDHamster.util.UUIDHelper;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;

public class LookupCommand extends Command {

	public LookupCommand(BasePlugin plugin, CommandManager commandManager) {
		super(plugin, commandManager);
	}

	@Override
	public void setPermissions(ArrayList<Permission> permissions) {
		permissions.add(new Permission("lookup"));
	}

	@Override
	public void setSubCommands(ArrayList<String[]> subCommands) {
		subCommands.add(new String[] { "lookup", "l" } );
	}

	@Override
	public void setDescription(StringBuilder description) {
		description.append("help.lookup");
	}

	@Override
	public void setArguments(ArrayList<Argument> arguments) {
		arguments.add(new Argument("help.args.username_uuid.description",   "help.args.username_uuid.placeholder"));
        arguments.add(new Argument("help.args.max.description",             "help.args.max.placeholder",            true));
        arguments.add(new Argument("help.args.timestamp.description",       "help.args.timestamp.placeholder",      true));
	}

	@Override
	public void execute(CommandSender commandSender, String[] subcommands, String[] args) throws CommandException, IllegalCommandArgumentException {
		if (args.length < 1 || args.length > 3) {
            throw new IllegalCommandArgumentException();
		}

        UUIDHamsterAPI api = new UUIDHamsterAPI();
        String in = args[0];

        for (int i = 1; i < args.length; i++) {
            Matcher maxMatcher = ArgumentMatcher.maxPattern.matcher(args[i]);
            Matcher timestampMatcher = ArgumentMatcher.timestampPattern.matcher(args[i]);

            if (maxMatcher.find()) {
                api.max(Integer.valueOf(maxMatcher.group("max")));

            } else if (timestampMatcher.find()) {
                try {
                    api.at(new Date(
                            Integer.valueOf(timestampMatcher.group("timestamp")) * 1000
                    ));
                } catch (Exception ignored) {}
            }
        }

		if (UUIDHelper.isUUID(in)) {
			PlayerProfile profile = api.profile(commandSender, UUIDHelper.toUUID(in), new LookupCommandCallback());
			if (profile != null) {
				new LookupCommandCallback().finished(commandSender, profile);
			}

		} else {
            List<PlayerProfile> profiles = api.profile(commandSender, in, new LookupCommandCallback());
            if (profiles != null) {
                new LookupCommandCallback().finished(commandSender, profiles);
            }
		}
	}

}