package org.lichtspiele.UUIDHamster.command;

import de.cubenation.bedrock.BasePlugin;
import de.cubenation.bedrock.command.Command;
import de.cubenation.bedrock.command.argument.Argument;
import de.cubenation.bedrock.command.manager.CommandManager;
import de.cubenation.bedrock.exception.CommandException;
import de.cubenation.bedrock.exception.IllegalCommandArgumentException;
import de.cubenation.bedrock.permission.Permission;
import org.bukkit.command.CommandSender;
import org.lichtspiele.UUIDHamster.api.UUIDHamsterAPI;
import org.lichtspiele.UUIDHamster.callback.plugin.ProfileDeleteCallback;

import java.util.ArrayList;

public class DeleteCommand extends Command {

    public DeleteCommand(BasePlugin plugin, CommandManager commandManager) {
        super(plugin, commandManager);
    }

    @Override
    public void setPermissions(ArrayList<Permission> permissions) {
        permissions.add(new Permission("delete"));
    }

    @Override
    public void setSubCommands(ArrayList<String[]> subCommands) {
        subCommands.add(new String[] { "delete", "d" });
    }

    @Override
    public void setDescription(StringBuilder description) {
        description.append("help.delete");
    }

    @Override
    public void setArguments(ArrayList<Argument> arguments) {
        arguments.add(new Argument("help.args.username_uuid.description", "help.args.username_uuid.placeholder"));
    }

    @Override
	public void execute(CommandSender sender, String[] subcommands, String[] args) throws CommandException, IllegalCommandArgumentException {
        // check args length
        if (args.length != 1) {
            throw new IllegalCommandArgumentException();
        }

        ProfileDeleteCallback callback = new ProfileDeleteCallback();
        new UUIDHamsterAPI().delete(sender, args[0], callback);
	}

}