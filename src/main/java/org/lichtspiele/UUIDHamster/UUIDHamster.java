package org.lichtspiele.UUIDHamster;

import de.cubenation.bedrock.BasePlugin;
import org.lichtspiele.UUIDHamster.command.DeleteCommand;
import org.lichtspiele.UUIDHamster.command.HistoryCommand;
import org.lichtspiele.UUIDHamster.command.LookupCommand;
import org.lichtspiele.UUIDHamster.command.SaveCommand;
import org.lichtspiele.UUIDHamster.config.Hamster;
import org.lichtspiele.UUIDHamster.config.locale.de_DE;
import org.lichtspiele.UUIDHamster.config.locale.en_US;
import org.lichtspiele.UUIDHamster.exception.ConfigurationPropertyMissingException;
import org.lichtspiele.UUIDHamster.listener.PlayerJoinListener;
import org.lichtspiele.UUIDHamster.tasks.SaveTask;

import java.util.ArrayList;
import java.util.HashMap;

public class UUIDHamster extends BasePlugin {

	public static UUIDHamster instance;

	/*
	 * instance stuff
	 */
	public static UUIDHamster getInstance() {
		return UUIDHamster.instance;
	}
	
	private void setInstance(UUIDHamster instance) {
		UUIDHamster.instance = instance;
	}


	/*
	 * Plugin enabling
	 */
	public void onPreEnable() {
		this.setInstance(this);
	}

	public void onPostEnable() {
		// check for API settings, disable plugin unless they exist
        try {
            if (((Hamster) this.getConfigService().getConfig(Hamster.class)).getApiKey().isEmpty())
                throw new ConfigurationPropertyMissingException("api.key");

			if (((Hamster) this.getConfigService().getConfig(Hamster.class)).getApiUri().isEmpty())
                throw new ConfigurationPropertyMissingException("api.uri");

        } catch (ConfigurationPropertyMissingException e) {
            this.disable(e);
            return;
        }

		// register events and timer task
		this.registerEvents();
		this.registerTimerTasks();
	}

    @Override
    public ArrayList<Class<?>> getCustomConfigurationFiles() {
        return new ArrayList<Class<?>>() {{
            add(Hamster.class);
            add(de_DE.class);
            add(en_US.class);
        }};
    }

    @Override
    public void setCommands(HashMap<String, ArrayList<Class<?>>> commands) {
        commands.put("hamster", new ArrayList<Class<?>>() {{
            add(HistoryCommand.class);
            add(LookupCommand.class);
            add(SaveCommand.class);
            add(DeleteCommand.class);
        }});
    }


    private void registerEvents() {
		this.getServer().getPluginManager().registerEvents(new PlayerJoinListener(), this);
	}	

	private void registerTimerTasks() {		
		long interval = ((Hamster) this.getConfigService().getConfig(Hamster.class)).getSaveInterval();
		
		this.getServer().getScheduler().runTaskTimer(this, new Runnable() {
			public void run() {
				new SaveTask().run();
			}
		}, 60L, (long) 20 * interval);
	}

}