package org.lichtspiele.UUIDHamster;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.lichtspiele.UUIDHamster.util.UUIDHelper;

import java.util.*;
import java.util.Map.Entry;
import java.util.logging.Level;

public class PlayerProfile {

    private String username                     = null;

    private UUID uuid                          = null;

	private boolean legacy						= true;
	
	private boolean cracked					= false;
    
    private HashMap<Date,String> history       = new HashMap<>();

	public PlayerProfile(JSONObject json) {
        // debug message
        UUIDHamster.getInstance().log(Level.FINER, json.toJSONString());

		// set name and uuid
		this.setUsername(	(String) json.get("username"));
		this.setUUID( 		UUIDHelper.toUUID( (String) json.get("uuid") ));

		// set account properties
		this.setLegacy( 	String.valueOf(json.get("legacy")).equals("1")	);
		this.setCracked(	String.valueOf(json.get("cracked")).equals("1")	);
		
		// set history
		JSONArray history = (JSONArray) json.get("history");
        for (Object aHistory : history) {
            JSONObject history_data = (JSONObject) aHistory;

            long changed_at = 0;
            if (history_data.get("changed_at") != null) {
                if (history_data.get("changed_at") instanceof String) {
                    changed_at = Long.valueOf((String) history_data.get("changed_at"));

                } else if (history_data.get("changed_at") instanceof Long) {
                    changed_at = (long) history_data.get("changed_at");

                } else {
                    UUIDHamster.getInstance().log(Level.SEVERE, String.format("changed_at returned unsupported class type '%s' for JSON String\n%s",
                            history_data.get("changed_at").getClass().toString(),
                            history_data.toJSONString()
                    ));
                    continue;

                }
            }

            this.history.put(
                    (changed_at == 0) ? null : new Date(changed_at),
                    (String) history_data.get("username")
            );
        }
    }

    private void setUsername(String username) {
        this.username = username;
    }

    public String getUsername() {
        return this.username;
    }

    
    private void setUUID(UUID uuid) {
        this.uuid = uuid;
    }
    
    public UUID getUUID() {
        return this.uuid;
    }

    @SuppressWarnings("unused")
    public Player getPlayer() {
        Player p = Bukkit.getServer().getPlayer(this.getUUID());
        return (p != null)
            ? p 
            : (Player) Bukkit.getServer().getOfflinePlayer(this.getUUID());
    }

	@SuppressWarnings("unused")
	public boolean isLegacy() {
		return this.legacy;
	}

	private void setLegacy(boolean legacy) {
		this.legacy = legacy;
	}

	@SuppressWarnings("unused")
	public boolean isCracked() {
		return this.cracked;
	}

	private void setCracked(boolean cracked) {
		this.cracked = cracked;
	}
    
    
    public boolean hasHistoricUsernames() {
        return (this.history.size() > 1);
    }

    public boolean hasHistoricUsername(String username) {
        if (!this.hasHistoricUsernames()) return false;

        for (Entry<Date, String> pair : history.entrySet()) {
            if (pair.getValue().equals(username)) return true;
        }

        return false;
    }

	@SuppressWarnings("unused")
    public Date getChangeDate(String username) {
    	if (!this.hasHistoricUsername(username)) return null;

        for (Entry<Date, String> pair : history.entrySet())
            if (pair.getValue().equals(username))
                return pair.getKey();
		
		return null;
    }
    
    public Map<Date,String> getHistory() {
        Map<Date, String> sorted = new TreeMap<>(new Comparator<Date>() {
            public int compare(Date date1, Date date2) {
                if (date1 == null) return 0;
                if (date2 == null) return 1;
                return date1.compareTo(date2);
            }
        });
        sorted.putAll(this.history);
        return sorted;
    }
   
}